import { randomComments } from "./botData"

export default () => {
    const comments = randomComments;
    return getRandomComment(comments);
}

const getRandomInt = (max: number) => {
    max = Math.floor(max);
    return Math.floor(Math.random() * max);
}

const getRandomComment = (comments: string[]) => {
    const randomInt = getRandomInt(comments.length);
    return comments[randomInt];
}
