import { SECONDS_FOR_GAME } from "../config";
import { IResult } from "../socketHelpers/results";
import { closeToFinishPhrase, finishPhrase } from "./botData";
import { getRaceProgress } from "./botGiveInfo"

interface IUserTime {
    username: string,
    seconds: number,
}



// when 30% of the track left for the first racer
// this function will be called

const mapResultTime: Map<string, IUserTime[]> = new Map();

export default (roomId: string, seconds: number) => {
    if (!mapResultTime.has(roomId)) mapResultTime.set(roomId, []);
    const roomUsers = mapResultTime.get(roomId)
    const results = getRaceProgress(roomId);
    results.forEach((res) => {
        if (res.progress === 100 && !findUserInMap(res.username, roomUsers!)) {
            roomUsers?.push({
                username: res.username,
                seconds: SECONDS_FOR_GAME - seconds,
            })
        }
    })
}

export const showFinishResults = (roomId: string) => {
    const phrase = finishPhrase;
    const phrasesArray: string[] = [];
    mapResultTime.get(roomId)?.forEach((user, index) => {
        if (index < 3) {
            phrasesArray.push(`${index + 1}. ${user.username} finished race for ${user.seconds} seconds.`);
        }
    })
    return phrase + phrasesArray.join('\n');
};

export const deleteTimeResults = (roomId: string) => mapResultTime.delete(roomId);



const findUserInMap = (usernameToFind: string, UsersInMap: IUserTime[]) => {
    const foundUser = UsersInMap.find((mapUser) => mapUser.username === usernameToFind);
    if (foundUser) return true;
    return false;
}


