import { closeToFinishPhrase } from "./botData";
import { getRaceProgress } from "./botGiveInfo"




// when 30% of the track left for the first racer
// this function will be called
const announcedStatusRoomSet = new Set<string>();

export default (roomId: string) => {
    if(announcedStatusRoomSet.has(roomId)) return;
    announcedStatusRoomSet.add(roomId);
    const results = getRaceProgress(roomId);
    const closeToFinish = closeToFinishPhrase;
    const phrasesArray: string[] = [];
    const phrase = `${results[0].username} is very near to finish this race. ` +
        `He passed ${results[0].progress}% of the track.`;
    phrasesArray.push(phrase);

    if (results.length > 1) {
        phrasesArray.push(`Then we have ${results[1].username} who passed ${results[1].progress}%`);
        if (results.length > 2) {
            phrasesArray.push(`This racer is chased by ${results[2].username}` +
                ' who passed ' +
                `${results[2].progress}% of the track`);
        }
    }
    return closeToFinish + phrasesArray.join(' ');
}

export const clearAnnounceStatus = (roomId: string) => {
    announcedStatusRoomSet.delete(roomId);
}

