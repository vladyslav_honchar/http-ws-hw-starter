import { roomsMap } from "../../helpers/roomHelper";
import { SECONDS_FOR_GAME } from "../config";
import { finishRoomResults, IResult, results } from "../socketHelpers/results";
import { textEveryThirtySeconds } from "./botData";


export const gatherInfo = (roomId: string, seconds: number) => {   
    return createMessage(getRaceProgress(roomId), seconds);
}

export const getRaceProgress = (roomId: string) => {
    const room = roomsMap.get(roomId);
    let notFullResult: IResult[] = [];
    room!.forEach((user) => {
        if (user.progress < 100)
            notFullResult.push({ username: user.username, progress: user.progress });
    });
    notFullResult.sort(compareIResult);
    let fullResults = results.get(roomId);
    if (!fullResults) fullResults = [];
    const tempResults = fullResults!.concat(notFullResult)
    return tempResults;
}

const createMessage = (tempResults: IResult[], seconds: number) => {
    const defaultTextBefore = textEveryThirtySeconds;
    const arrayResultsToString: string[] = [];
    tempResults.forEach((res, index) => {
        arrayResultsToString.push(`${index + 1}. ${res.username} has passed ${res.progress}% of the track`);
    });
    const timeLeft = `\nOnly ${seconds}s left to finish the race.`;
    return defaultTextBefore + arrayResultsToString.join('\n') + timeLeft;
}



function compareIResult(a: IResult, b: IResult) {
    return (a.progress - b.progress) * -1;
}


