import { roomsMap } from "../../helpers/roomHelper";
import { startGameSpeech, carsNames } from "./botData";

const getRandomInt = (max: number) => {
    max = Math.floor(max);
    return Math.floor(Math.random() * max);
}

const getRandomCar = ():string => {
    return carsNames[getRandomInt(carsNames.length)];
}
export default (roomId: string) => {
    const room = roomsMap.get(roomId);
    const startSpeech = startGameSpeech;
    const usersAndTheirCars: string[] = [];
    room!.forEach((user) => {
        usersAndTheirCars.push(`${user.username} is on a ${getRandomCar()}`);
    });
    const finalStartSpeech:string = startGameSpeech + usersAndTheirCars.join('\n');
    return finalStartSpeech;
}