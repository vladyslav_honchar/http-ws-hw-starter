export const startGameSpeech = 'Hello everybody! ' +
    'This is a Typing Championship ' +
    'Please welcome our drivers to the race\n';

export const carsNames: string[] = ['Porsche', 'ZAZ',
    'Mercedes', 'BMW',
    'Lada', 'Ferrari',
    'Renault', 'Toyota',
    'Honda', 'Ford',
    'Nissan', 'Volkswagen'];

export const textEveryThirtySeconds = 'We are watching a very interesting race. ' +
    'I want to announce the situation on the track at the moment.\n';

export const closeToFinishPhrase = 'The end of the race for the first racer is very near,' +
    'let\'s summarize a little.\n';

export const finishPhrase = 'So the race is over. Here are the results:\n';

export const randomComments: string[] = [
    'The french canadian keyboard layout is used in french-speaking parts of Canada, ' +
    'mainly the Quebec region. It features the "snow mode" key, with snowflake icon ' +
    'next to Ctrl caption. It looks funny, because we usually tend to associate Canada ' +
    `with winter time (although it's the same stereotype that all Italians eat pasta only).`,

    'Space is the longest key on every keyboard, but in Japan the statistical Spacebar is much ' +
    'shorter than in any other place on Earth. Why? Because japanese keyboards require switching ' +
    'between latin/roman letters (called rōmaji) and japanese characters (called hiragana and kana). ' +
    'To do that switching, additional keys are required. They are placed next to Space.',

    'When you hit spacebar, 600000 people in the world did just the same',

    'QWERTY is not the most efficient keyboard layout',

    'Keys that are (rather) useless but still on keyboards today:\n1. System Request\n' +
    '2. Print Screen\n3. Pause/Break',

    'The letters on the middle row, also known as the home row, do a total of 70% of the work. ' + 
    'The least common letters were on the bottom row, because it is the hardest row to reach.'
]