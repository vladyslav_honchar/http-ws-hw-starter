import { closeToFinishPhrase } from "./botData";
import { getRaceProgress } from "./botGiveInfo"




// when 30% of the track left for the first racer
// this function will be called

export default (roomId: string, username: string) => {
    const results = getRaceProgress(roomId);
    let place: number = 1;
    results.forEach((user, index) => {
        if (user.username === username) {
            place = index + 1;
            return;
        }
    })

    const phrase = `Racer ${username} finished the race. Position ${place} in the results table`;

    return phrase;
}

