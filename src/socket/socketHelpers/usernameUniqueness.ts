import { Socket } from "socket.io";

const usersSet = new Set<string>()

export default (socket: Socket) => {
    socket.on('CHECK_UNIQUENESS', (username: string) => {
        if (usersSet.has(username)) {
            socket.emit('IS_NOT_UNIQUE');
        } else {
            usersSet.add(username);
        }

    });
}

export const deleteUser = (username: string) => {
    usersSet.delete(username);
}