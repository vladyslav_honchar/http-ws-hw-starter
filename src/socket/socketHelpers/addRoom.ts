import { Server, Socket } from "socket.io";
import { IUser, roomsMap } from "../../helpers/roomHelper";

export default (io: Server, socket: Socket) => {
    socket.on('ADD_ROOM_REQ', (roomId: string): void => {
        if (roomsMap.has(roomId)) {
            socket.emit('ADD_ROOM_BAD_RES', roomId);
            return;
        }
        
        createRoom(socket, roomId);

        io.emit('UPDATE_ADDED_ROOM', roomId);
        socket.emit('ADD_ROOM_RES', roomId);
    });
    
};

export const createRoom = (socket: Socket, roomId: string): void => {
    const set: Set<IUser> = new Set();
    roomsMap.set(roomId, set);
};