import { Server, Socket } from "socket.io";
import {
  getCurrentRoomId,
  clearAllRoomTimers,
  setRoomStatus,
  setDefaultRoom,
  roomsMap,
} from "../../helpers/roomHelper";
import { finishRoomResults, getRoomResults, clearResults } from "./results";
import { MAXIMUM_USERS_FOR_ONE_ROOM as maxUsers } from "../config";
import { clearAnnounceStatus } from "../bot/botToFinish";
import { deleteTimeResults, showFinishResults } from "../bot/botResults";
export default (io: Server, socket: Socket) => {};

export function stopGame(io: Server, socket: Socket, roomName?: string) {
  let roomId: string | undefined;
  if (!roomName) {
    roomId = getCurrentRoomId(socket);
  } else {
    roomId = roomName;
  }
  if (roomId) {
    clearAllRoomTimers(roomId);
    finishRoomResults(roomId);
    clearAnnounceStatus(roomId);
    const results = getRoomResults(roomId);
    io.to(roomId!).emit('BOT_SHOW_FINISH', showFinishResults(roomId));
    deleteTimeResults(roomId);
    clearResults(roomId);
    io.to(roomId!).emit('STOP_GAME', results);
    setRoomStatus(roomId, 'created');
    setDefaultRoom(roomId);
    const room = roomsMap.get(roomId!);
    room!.forEach((user) => {  
      io.in(roomId!).emit('CHECK_READY', user.username, user.ready);
      io.in(roomId!).emit('CHANGE_USER_PROGRESS', user.username, 0);
    });
    if (room!.size < maxUsers && room!.size > 0) io.emit('APPEAR_ROOM', roomId);
  }
}
