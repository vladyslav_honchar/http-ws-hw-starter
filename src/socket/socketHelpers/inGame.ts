import { Socket, Server } from "socket.io";
import {
  findUserInRoom,
  IUser,
  getCurrentRoomId,
  roomsMap,
  setRoomStatus,
  addTimerToRoom,
} from "../../helpers/roomHelper";
import { SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "../config";
import { texts } from "../../data";
import { setTextLength } from "./gameProgress";
import { stopGame } from "./stopGame";
import botStartGame from "../bot/botStartGame";
import { gatherInfo } from "../bot/botGiveInfo";
import sendBotResults from "../bot/botResults";
import botRandomComment from "../bot/botRandomComment";

export default (io: Server, socket: Socket) => {
  socket.on('CHANGE_READY', (): void => {
    const roomId = getCurrentRoomId(socket);
    const username = socket.handshake.query.username;
    io.in(roomId!).emit('GET_READY_STATUS', username);
    changeReady(socket);
    if (canGameStart(socket)) startGame(io, socket, roomId!);
  });
  socket.on("IS_READY", (username): void => {
    const roomId = getCurrentRoomId(socket);
    const room = roomsMap.get(roomId!);
    room!.forEach((user) => {
      if (user.username == username) {
        socket.emit('CHECK_READY', username, user.ready);
        return;
      }
    });
  });
};

function changeReady(socket: Socket) {
  const user: IUser | undefined = findUserInRoom(socket);
  if (user) {
    user.ready = !user.ready;
  }
}

function canGameStart(socket: Socket): boolean {
  const roomId = getCurrentRoomId(socket);
  const room = roomsMap.get(roomId!);
  if (room) {
    let flag = true;
    room.forEach((user: IUser) => {
      if (!user.ready) flag = false;
    });
    return flag;
  } else {
    return false;
  }
}

function startGame(io: Server, socket: Socket, roomId: string) {
  setRoomStatus(roomId, "started");
  io.emit("DISAPPEAR_ROOM", roomId);
  const textId = randomInteger(0, texts.length - 1);
  setTextLength(textId);
  io.in(roomId).emit('BOT_START_GAME', botStartGame(roomId));
  io.in(roomId).emit('START_GAME');
  io.in(roomId).emit("GET_TEXT", textId);
  startTimerBeforeGame(io, socket, roomId);
}

function startTimerBeforeGame(io: Server, socket: Socket, roomId: string) {
  let seconds = SECONDS_TIMER_BEFORE_START_GAME;
  let timerId = setTimeout(function tick() {
    io.in(roomId).emit("ALTER_TIMER_BEFORE_START", seconds);
    if (seconds == 0) {
      startGameProcess(io, socket, roomId);
      return;
    }
    seconds -= 1;
    timerId = setTimeout(tick, 1000);
    addTimerToRoom(roomId, timerId);
  }, 1000);
  addTimerToRoom(roomId, timerId);
}

function startGameProcess(io: Server, socket: Socket, roomId: string) {
  let seconds = SECONDS_FOR_GAME - 1;
  let secondsToGiveInfo = seconds - 30 + 1;
  const arrayWithSeconds: number[] = [];
  while (secondsToGiveInfo > 0) {
    arrayWithSeconds.push(secondsToGiveInfo);
    secondsToGiveInfo -= 30;
  }
  io.in(roomId).emit("ALTER_GAME_TIMER", seconds + 1);
  let isTimeToGiveInfo = false;
  let timerId = setTimeout(function tick() {
    sendBotResults(roomId, seconds);
    io.in(roomId).emit("ALTER_GAME_TIMER", seconds);
    if (seconds === 0) {
      stopGame(io, socket);
      return;
    }
    if (arrayWithSeconds.find((sec) => sec === seconds)) {
      io.in(roomId).emit('BOT_GIVE_INFO', gatherInfo(roomId, seconds));
      isTimeToGiveInfo = true;
    }
    if (!isTimeToGiveInfo && seconds > 5 && seconds % 8 === 0) {
      io.in(roomId).emit('BOT_RANDOM_COMMENT', botRandomComment());
    }
    isTimeToGiveInfo = false;
    seconds -= 1;
    timerId = setTimeout(tick, 1000);
    addTimerToRoom(roomId, timerId);
  }, 1000);
  addTimerToRoom(roomId, timerId);

}

function randomInteger(min: number, max: number): number {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
}
