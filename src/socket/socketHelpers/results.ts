import { roomsMap } from "../../helpers/roomHelper";

export interface IResult {
  username: string;
  progress: number;
}

export const results: Map<string, IResult[]> = new Map();

export function getRoomResults(roomId: string): IResult[] | undefined {
  const result = results.get(roomId);
  if (!result) return [];
  return results.get(roomId);
}

export function addUserToResults(roomId: string, username: string) {
  const room = roomsMap.get(roomId);
  let currentResults = results.get(roomId);
  if (!currentResults) currentResults = [];
  room!.forEach((user) => {
    if (user.username == username) {
      currentResults!.push({ username, progress: user.progress });
    }
  });
  results.set(roomId, currentResults);
}

export function finishRoomResults(roomId: string) {
  const room = roomsMap.get(roomId);
  let notFullResult: IResult[] = [];
  room!.forEach((user) => {
    if (user.progress < 100)
      notFullResult.push({ username: user.username, progress: user.progress });
  });
  notFullResult.sort(compareIResult);
  let fullResults = results.get(roomId);
  if (!fullResults) fullResults = [];
  results.set(roomId, fullResults!.concat(notFullResult));
}

function compareIResult(a: IResult, b: IResult) {
  return (a.progress - b.progress) * -1;
}

export function clearResults(roomId: string) {
  results.set(roomId, []);
}

export function deleteUserFromResults(roomId: string, username: string) {
  let result = getRoomResults(roomId);
  let usernameIndex: number = -1;
  for (let i = 0; i < result!.length; i++) {
    if (result![i].username == username) {
      usernameIndex = i;
    }
  }
  if (usernameIndex != -1) {
    result!.splice(usernameIndex, 1);
    results.set(roomId, result!);
  }
}
