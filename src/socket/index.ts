import { Server, Socket } from "socket.io";
import usernameUniqueness from "./socketHelpers/usernameUniqueness";
import disconnect from './socketHelpers/disconnection';
import getRoomsEvent from "./socketHelpers/getRooms";
import addRoomEvent from "./socketHelpers/addRoom";
import joinRoomEvent from "./socketHelpers/joinRoom";
import gameProgress from "./socketHelpers/gameProgress";
import leaveRoom from "./socketHelpers/leaveRoom";
import stopGame from "./socketHelpers/stopGame";
import inGame from "./socketHelpers/inGame";
import onlineCheckEvent from "./socketHelpers/onlineCheck";
import botAnnouncer from "./bot/index";
const userSet = new Set;

export default (io: Server) => {
    io.on("connection", (socket: Socket) => {
        const username = socket.handshake.query.username;

        usernameUniqueness(socket);
        disconnect(io, socket, username);
        getRoomsEvent(io, socket);
        addRoomEvent(io, socket);
        onlineCheckEvent(io, socket);
        joinRoomEvent(io, socket);
        leaveRoom(io, socket);
        gameProgress(io, socket);
        inGame(io, socket);
        stopGame(io, socket);
        botAnnouncer(io, socket);



    })
};