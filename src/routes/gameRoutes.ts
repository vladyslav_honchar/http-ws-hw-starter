import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import {texts} from "../data";

const router = Router();

router
  .get('/texts/:id', (req, res) => {
    const id = parseInt(req.params.id, 10);
    const textObj = {data: texts[id]}
    res.send(textObj)
  })
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  });

export default router;
