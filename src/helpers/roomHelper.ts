import { Socket } from "socket.io";

export interface IUser {
  id: string;
  username: string;
  ready: boolean;
  progress: number;
}

export const roomsMap: Map<string, Set<IUser>> = new Map();
export const roomStatus: Map<string, string> = new Map();
export const roomTimers: Map<
  string,
  ReturnType<typeof setTimeout>[]
> = new Map();

export function setRoomStatus(roomId: string, status: string) {
  roomStatus.set(roomId, status);
}

export function getRoomStatus(roomId: string) {
  const status = roomStatus.get(roomId);
  if (!status) return "created";
  return roomStatus.get(roomId);
}

export function addTimerToRoom(
  roomId: string,
  timer: ReturnType<typeof setTimeout>
) {
  let timers = roomTimers.get(roomId);
  if (!timers) {
    const newTimers: ReturnType<typeof setTimeout>[] = [];
    newTimers.push(timer);
    roomTimers.set(roomId, newTimers);
  } else {
    timers!.push(timer);
  }
}

export function clearAllRoomTimers(roomId: string) {
  const timers = roomTimers.get(roomId);
  if (timers) {
    for (let i = 0; i < timers.length; i++) {
      clearTimeout(timers[i]);
    }
    roomTimers.set(roomId, []);
  }
}

export const getCurrentRoomId = (socket: Socket) =>
  Object.keys(socket.rooms).find((roomId) => roomsMap.has(roomId));

export function findUserInRoom(socket: Socket): IUser | undefined {
  const roomId = getCurrentRoomId(socket);
  if (roomId) {
    const usersInRoom = roomsMap.get(roomId);
    let needUser: IUser | undefined;
    usersInRoom!.forEach((user: IUser) => {
      if (user.id == socket.id) {
        needUser = user;
        return;
      }
    });
    return needUser;
  } else {
    return undefined;
  }
}

export function setDefaultRoom(roomId: string) {
  const room = roomsMap.get(roomId);
  room!.forEach((user) => {
    user.progress = 0;
    user.ready = false;
  });
}
