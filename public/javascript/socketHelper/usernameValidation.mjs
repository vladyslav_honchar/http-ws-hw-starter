export default (socket, username) => {
    socket.emit('CHECK_UNIQUENESS', username);
    
    socket.on('IS_NOT_UNIQUE', () => {
        alert('Username is not unique');
        sessionStorage.clear();
        window.location.replace('/login');
    });
}