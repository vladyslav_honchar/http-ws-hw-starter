export default (socket) => {
  socket.on("CHANGE_USER_PROGRESS", (username, progress) => {
    const progressBar = document.getElementById(`${username}-progress`);
    changeProgressBar(progressBar, progress);
  });
};

function changeProgressBar(progressBar, progress) {
  progressBar.style.width = `${progress}%`;
  changeProgressBarColor(progressBar, progress);
}

function changeProgressBarColor(progressBar, progress) {
  if (progress === 100) {
    progressBar.style.backgroundColor = "blue";
  }
  if (progress < 100) {
    progressBar.style.backgroundColor = "green";
  }
}
