import { keyDownFunc } from "../helpers/changeTextHelper.mjs";
import { showModal } from "../helpers/modal.mjs";

export default (socket) => {
  socket.on("STOP_GAME", (results) => {
    document.removeEventListener("keydown", keyDownFunc);
    const arrResults = results.map((res, i) => {
      return `${i + 1}. ${res.username} ${res.progress}%`;
    });
    const stringResults = arrResults.join('\n')
    const modalElement = {
      title: "Results",
      bodyElement: stringResults,
    }
    //showModal(modalElement)
    setDefaultProperties(socket);
  });
};

function setDefaultProperties(socket) {
  hideGameProcess();
  showRoomButtons();
}
function hideGameProcess() {
  const gameTimer = document.getElementById("game-timer");
  const textBlock = document.getElementById("text-container");
  gameTimer.remove();
  textBlock.remove();
}
function showRoomButtons() {
  const readyButton = document.getElementById("ready-btn");
  const backButton = document.getElementById("quit-room-btn");
  readyButton.style.display = "block";
  backButton.style.display = "block";
  readyButton.innerText = "READY"
}

