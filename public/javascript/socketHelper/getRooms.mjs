import addRoomToPage from "../helpers/addRoomToPage.mjs";
import addClickListenerJoinRoom from "../helpers/joinButton.mjs";


export default (socket) => {
    socket.emit('GET_ROOMS_REQ');
    socket.on('GET_ROOMS_RES', (rooms) => {
        rooms.forEach(room => {
            addRoomToPage(room);
            socket.emit("ONLINE_IN_ROOM", room);
            addClickListenerJoinRoom(socket, room);
        });

    });
}