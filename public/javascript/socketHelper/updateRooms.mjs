import addRoomToPage from "../helpers/addRoomToPage.mjs"

export default (socket) => {
  socket.on("DISAPPEAR_ROOM", (roomId) => {
    const disappearedRoom = document.getElementById(`${roomId}-room-card`);
    disappearedRoom.style.display = "none";
  });
  socket.on("APPEAR_ROOM", (roomId) => {
    const appearedRoom = document.getElementById(`${roomId}-room-card`);
    if (appearedRoom) {
      appearedRoom.style.display = "flex";
    } else {
      addRoomToPage(roomId);
    }
  });
  socket.on("DELETE_ROOM", (roomId) => {
    const destroyedRoom = document.getElementById(`${roomId}-room-card`);
    destroyedRoom.remove();
  });
};
