export default (socket) => {
    socket.on("GET_ROOM_ONLINE", (roomId, online) => {
        const responseOnline = document.getElementById(`${roomId}-online`);
        responseOnline.innerText = `${online} in room`;
    });
};
