import displayMessage from "./displayMessage.mjs";

export default (io, socket) => {
    socket.on('START_BOT_ANNOUNCER', (username) => {
        displayMessage(
            `Greetings to ${username}`,
            true
        );
    })

}