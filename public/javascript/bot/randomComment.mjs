import displayMessage from "./displayMessage.mjs";

export default (io, socket) => {
    socket.on('BOT_RANDOM_COMMENT', (message) => {
        if (message) displayMessage(message, false, true);
    })

}