import displayMessage from "./displayMessage.mjs"

export default (io, socket) => {
    socket.on('GOODBYE_TO_USER', (username) => {
        displayMessage(
            `${username} decided not to take part in competition`,
            true
        );
    })

}