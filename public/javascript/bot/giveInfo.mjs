import displayMessage from "./displayMessage.mjs"

export default (io, socket) => {
    socket.on('BOT_GIVE_INFO', (message) => {
        if (message) displayMessage(message, false);
    })

}