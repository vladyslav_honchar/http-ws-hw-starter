import greetingsToUsersInRoom from "./greetings.mjs";
import goodbyeToUserThatLeaves from "./goodbye.mjs";
import startGame from "./startGame.mjs";
import giveInfo from "./giveInfo.mjs";
import nearFinish from "./nearFinish.mjs";
import onFinish from "./onFinish.mjs";
import finishResult from "./finishResult.mjs";
import randomComment from "./randomComment.mjs";

export default (io, socket) => {
    greetingsToUsersInRoom(io, socket);
    goodbyeToUserThatLeaves(io, socket);
    startGame(io, socket);
    giveInfo(io, socket);
    nearFinish(io, socket);
    onFinish(io, socket);
    finishResult(io, socket);
    randomComment(io, socket);
}