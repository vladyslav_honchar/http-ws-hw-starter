import displayMessage from "./displayMessage.mjs";

export default (io, socket) => {
    socket.on('BOT_START_GAME', (message) => {
        displayMessage(message, false);
    })

}