export default (message, canBeInterruped, isComment) => {
    const botComments = document.getElementById('bot-comments');
    if (isComment) {
        botComments.innerText = message;
    }
    else if (canBeInterruped) {
        setTimeout(() => {
            botComments.innerText = message;
        }, 1000);
    } else {
        setTimeout(() => {  
            botComments.innerText = message;
        }, 500);

        setTimeout(() => {
            botComments.innerText = message;
        }, 2500);
    }
}