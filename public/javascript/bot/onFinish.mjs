import displayMessage from "./displayMessage.mjs";

export default (io, socket) => {
    socket.on('BOT_ON_FINISH', (message) => {
        if (message) displayMessage(message, false);
    })

}