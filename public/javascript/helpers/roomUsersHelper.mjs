import { createElement } from "./createElement.mjs";

const userBlock = document.getElementById("users-unit");

export default function addUserInRoom(username) {
    const userCard = createElement({
        tagName: "div",
        className: "user-card",
        attributes: {
            id: `${username}-user-card`,
        },
    });

    const usernameBlock = createElement({
        tagName: "div",
        className: "username-block",
    });

    const usernameElem = createElement({
        tagName: "div",
        className: "username-span",
        attributes: {
            id: `${username}-name-elem`,
        },
    });
    usernameElem.innerText = username;

    const readyStatus = createElement({
        tagName: "div",
        className: "rdy-status",
        attributes: {
            id: `${username}-ready`,
        },
    });

    const progressBar = createElement({
        tagName: "div",
        className: "progress-bar",
    });
    const progress = createElement({
        tagName: "div",
        className: "progress",
        attributes: {
            id: `${username}-progress`,
        },
    });

    usernameBlock.append(usernameElem);
    usernameBlock.append(readyStatus);

    progressBar.append(progress);

    userCard.append(usernameBlock);
    userCard.append(progressBar);

    userBlock.append(userCard);
}

export function changeReady(socket, username) {
    socket.emit("CHANGE_READY");
    const readyButton = document.getElementById(`ready-btn`);
    if (readyButton.innerText == "NOT READY") {
        readyButton.innerText = "READY";
    } else {
        readyButton.innerText = "NOT READY";
    }
}

export function showGamePage(socket, username, roomName) {
    const roomInfo = createRoomInfoElement(socket, roomName);
    const roomPage = document.getElementById("rooms-page");
    const gamePage = document.getElementById("game-page");
    roomPage.style.display = "none";
    gamePage.style.display = "flex";
    const gameBlock = document.getElementById("game-block");
    const readyButton = createElement({
        tagName: "button",
        className: "ready-btn btn",
        attributes: {
            id: `ready-btn`,
        },
    });
    readyButton.innerText = "READY";
    readyButton.addEventListener("click", () => changeReady(socket, username));
    gameBlock.append(readyButton);

    const userNameElem = document.getElementById(`${username}-name-elem`);
    userNameElem.innerText += ' (you)';
}

export function DeleteUserFromRoom(username) {
    const userCard = document.getElementById(`${username}-user-card`);
    userCard.remove();
}

function createRoomInfoElement(socket, roomName) {
    const roomInfo = document.getElementById("room-info");
    const backButton = createElement({
        tagName: "button",
        className: "quit-room-btn btn",
        attributes: {
            id: "quit-room-btn",
        },
    });
    backButton.innerText = "Back To Rooms";
    const roomNameElem = createElement({
        tagName: "div",
        className: "room-name",
    });
    roomNameElem.innerText = `Room: ${roomName}`;
    backButton.addEventListener("click", () => backToLobby(socket));
    roomInfo.append(backButton);
    roomInfo.append(roomNameElem);
    return roomInfo;
}

function backToLobby(socket) {
    const roomPage = document.getElementById("rooms-page");
    const gamePage = document.getElementById("game-page");
    const roomInfo = document.getElementById("room-info");
    const usersBlock = document.getElementById("users-unit");
    const gameBlock = document.getElementById("game-block");
    roomInfo.innerHTML = "";
    gameBlock.innerHTML = "";
    usersBlock.innerHTML = "";
    roomPage.style.display = "block";
    gamePage.style.display = "none";
    socket.emit("LEAVE_FROM_ROOM");
}
